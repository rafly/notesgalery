package com.example.notesgalery;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<DataClass> dataList;
    private Context context;
    final String tag ="MyAdapter";

    public MyAdapter(Context context, ArrayList<DataClass> dataList) {

        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Glide.with(context).load(dataList.get(position).getImageURL()).into(holder.recyclerImage);
        holder.recyclerCaption.setText(dataList.get(position).getCaption());
        holder.btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotesActivity.class);
                // Mengirim URL gambar ke NotesActivity
                intent.putExtra("image_url", dataList.get(position).getImageURL());
                Log.d(tag, "image_url" + intent);
                intent.putExtra("image_id", dataList.get(position).getId());
                Log.d(tag, "image_id" + intent);
                context.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Panggil fungsi untuk menghapus data gambar
                deleteImage(dataList.get(position).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView recyclerImage, delete;
        TextView recyclerCaption;
        Button btEdit;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            recyclerImage = itemView.findViewById(R.id.recyclerImage);
            recyclerCaption = itemView.findViewById(R.id.recyclerCaption);
            btEdit = itemView.findViewById(R.id.btEdit);
            delete = itemView.findViewById(R.id.btDelete);
        }
    }

    private void deleteImage(String imageId) {
        // Hapus data dari Firebase Realtime Database
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUID = currentUser.getUid();
        Log.d("MyAdapter", "currentUserUID = " + currentUserUID);
        Log.d("MyAdapter", "imageID = " + imageId);
//        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(currentUserUID).child(imageId);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Images").child(currentUserUID).child(imageId);


        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Mendapatkan nilai dari dataSnapshot
                String imageName = dataSnapshot.child("imageName").getValue(String.class);

                // Sekarang kamu dapat menggunakan nilai caption, diary, dan url sesuai kebutuhan
                // Misalnya, mencetak nilai ke logcat
                if(imageName != null){

                    ///---
                    // Hapus gambar dari Firebase Storage
                    StorageReference storageReference = FirebaseStorage.getInstance("gs://tugasakhir-8a683.appspot.com").getReference().child(currentUserUID).child(imageName);
                    storageReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d("isDeleted","deleted");
                            // Gambar dan data dihapus dengan sukses
                            // Tambahkan kode yang sesuai, misalnya menampilkan pesan sukses atau memperbarui tampilan
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Gagal menghapus gambar
                            Log.d("isDeleted","not deleted");
                            // Tambahkan kode yang sesuai, misalnya menampilkan pesan kesalahan
                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle kegagalan baca data, jika diperlukan
                Log.e("FirebaseError", "Error: " + databaseError.getMessage());
            }
        });

        databaseReference.removeValue();



    }


}
