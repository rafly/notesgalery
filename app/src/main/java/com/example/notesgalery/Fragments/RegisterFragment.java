package com.example.notesgalery.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

//import com.example.notesgalery.LoginActivity;
import com.example.notesgalery.R;
//import com.example.notesgalery.RegisterActivity;
import com.example.notesgalery.RegisterLoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    EditText regisFullName, regisEmail, regisPassword;
    Button btRegis_toLogin;
    ImageView back;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        regisEmail = view.findViewById(R.id.tietEmail);
        regisPassword = view.findViewById(R.id.tietPassword);
        btRegis_toLogin = view.findViewById(R.id.btRegis_ToLogin);
        regisFullName = view.findViewById(R.id.tietFullName);
        firebaseAuth = FirebaseAuth.getInstance();
//        back = view.findViewById(R.id.btBack_toLogin);

        btRegis_toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = regisEmail.getText().toString();
                String password = regisPassword.getText().toString();
                String fullName = regisFullName.getText().toString();
                register(email, password, fullName);
            }
        });

        return view;
    }

    public void register(String email, String password, final String fullName) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            if (user != null) {
                                String uid = user.getUid();
                                // Simpan data pengguna ke Firebase Realtime Database
                                saveUserData(uid, fullName, email, password);
                            }
                            Toast.makeText(getActivity(), "Registrasi berhasil. Selamat datang " + email, Toast.LENGTH_SHORT).show();
                            Intent toLogin = new Intent(getActivity(), RegisterLoginActivity.class);
                            startActivity(toLogin);
                            getActivity().finish();
                        } else {
                            Toast.makeText(getActivity(), "Registrasi gagal. Email mungkin sudah digunakan atau terjadi kesalahan lain.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void saveUserData(String uid, String fullName, String email, String password) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users").child(uid);

        // Simpan data pengguna ke Firebase Realtime Database
        usersRef.child("fullName").setValue(fullName);
        usersRef.child("email").setValue(email);
        usersRef.child("password").setValue(password);
    }

}