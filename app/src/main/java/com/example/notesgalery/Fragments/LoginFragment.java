package com.example.notesgalery.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.notesgalery.LoginActivity;
import com.example.notesgalery.MainActivity;
import com.example.notesgalery.R;
//import com.example.notesgalery.RegisterActivity;
import com.example.notesgalery.RegisterLoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFragment extends Fragment {

    private FirebaseAuth fireBaseAuth;
    Button btLogin, btRegister;
    EditText loginEmail, loginPassword;
    TextView toRegister;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        fireBaseAuth = FirebaseAuth.getInstance();
        btLogin = view.findViewById(R.id.btLogin_ToMain);
        loginEmail = view.findViewById(R.id.tietEmail_login);
        loginPassword = view.findViewById(R.id.tietPassword_login);
        toRegister = view.findViewById(R.id.tvTo_Register);

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = loginEmail.getText().toString();
                String password = loginPassword.getText().toString();
                login(email, password);
            }

            public void login(String email, String password) {
                fireBaseAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getActivity(), "Maaf, email dan password Anda salah", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        return view;
    }

}