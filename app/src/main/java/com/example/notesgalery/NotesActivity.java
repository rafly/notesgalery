package com.example.notesgalery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class NotesActivity extends AppCompatActivity {

    Button btSave;
    ImageButton btEdit;
    TextView textDiary, caption;
    DatabaseReference databaseReference; // Referensi ke Firebase Database
    String imageId; // ID gambar yang sedang dilihat

    final String tag = "NotesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        // Inisialisasi elemen tampilan dan referensi database
        btEdit = findViewById(R.id.editButtonDiary);
        btSave = findViewById(R.id.saveButtonDiary);
        textDiary = findViewById(R.id.textDiary);
//        caption = findViewById(R.id.captionNotes);

        //Firebase
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String currentUserUID = currentUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference("Images").child(currentUserUID);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra("image_url");
        imageId = intent.getStringExtra("image_id");

        Log.d(tag, "imageId init = " + imageId);
        //
        if (imageId != null) {
            DatabaseReference diaryReference = databaseReference.child(imageId).child("diary");
            diaryReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String diaryText = dataSnapshot.getValue(String.class);
                    if (diaryText != null) {
                        // Jika ada diary yang tersimpan, tampilkan di textDiary
                        textDiary.setText(diaryText);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Handle error jika terjadi kesalahan saat mengambil data dari Firebase
                    Log.e(tag, "Error retrieving diary data: " + databaseError.getMessage());
                }
            });
        }
        //
        if (imageUrl != null) {
            ImageView imageView = findViewById(R.id.imageView);
            Glide.with(this).load(imageUrl)
                    .centerCrop()
                    .into(imageView);
        }

        btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textDiary.setEnabled(true);
                btEdit.setVisibility(View.GONE);
                btSave.setVisibility(View.VISIBLE);
            }
        });

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textDiary.setEnabled(false);
                btEdit.setVisibility(View.VISIBLE);
                btSave.setVisibility(View.GONE);
                String diaryText = textDiary.getText().toString();
                saveDiaryToFirebase(diaryText);
            }
        });


    }

    private void saveDiaryToFirebase(String diaryText) {
        Log.d(tag, "imageId save to database = " + imageId);
        // Update keterangan (diary) di Firebase Database
        if (imageId != null) {
//            databaseReference.child(imageId).child("caption").setValue(caption);
            databaseReference.child(imageId).child("diary").setValue(diaryText);
            Toast.makeText(this, "Diary saved", Toast.LENGTH_SHORT).show();
        }
    }
}
