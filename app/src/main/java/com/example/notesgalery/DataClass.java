package com.example.notesgalery;

public class DataClass {
    private String imageURL, caption, diary, id, imageName;

    public DataClass(){

    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDiary() {
        return diary;
    }

    public void setDiary(String diary) {
        this.diary = diary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public DataClass(String imageURL, String caption, String diary, String imageName) {
        this.imageURL = imageURL;
        this.caption = caption;
        this.diary = diary;
        this.imageName = imageName;
    }

//    public DataClass(String imageURL, String caption, String diary, String id) {
//        this.imageURL = imageURL;
//        this.caption = caption;
//        this.diary = diary;
//        this.id = id;
//    }
}