//package com.example.notesgalery;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.view.GravityCompat;
//import androidx.drawerlayout.widget.DrawerLayout;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//
//import java.util.ArrayList;
//
//public class MainActivity extends AppCompatActivity {
//
//    FloatingActionButton fab;
//    private RecyclerView recyclerView;
//    private ArrayList<DataClass> dataList;
//    private MyAdapter adapter;
//    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Images");
//    Button edit;
//
//    //navbar + toolbar
//    DrawerLayout drawerLayout;
//    ImageView menu, close;
//    LinearLayout favorite, notes, npn, settings, logout;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        fab = findViewById(R.id.fab);
//
//        recyclerView = findViewById(R.id.recyclerView);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        dataList = new ArrayList<>();
//        adapter = new MyAdapter(this, dataList);
//        recyclerView.setAdapter(adapter);
//
//        //navbar + toolbar
//        drawerLayout = findViewById(R.id.drawerLayout);
//        menu = findViewById(R.id.btMenu);
//        favorite = findViewById(R.id.favorite);
//        notes = findViewById(R.id.notes);
//        npn = findViewById(R.id.npn);
//        settings = findViewById(R.id.settings);
//        logout = findViewById(R.id.logout);
//        close = findViewById(R.id.btCloseNavbar);
//
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                closedDrawer(drawerLayout);
//            }
//        });
//        menu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openDrawer(drawerLayout);
//            }
//        });
//        notes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                recreate();
//            }
//        });
//        settings.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                redirectActivity(MainActivity.this, SettingsActivity.class);
//            }
//        });
//
//
//
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
//                    DataClass dataClass = dataSnapshot.getValue(DataClass.class);
//                    dataList.add(dataClass);
//                }
//                adapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });
//
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, UploadActivity.class);
//                startActivity(intent);
////                finish();
//            }
//        });
//    }
//
//    //navbar + toolbar
//    public static void openDrawer(DrawerLayout drawerLayout){
//        drawerLayout.openDrawer(GravityCompat.END);
//    }
//
//    public static void closedDrawer(DrawerLayout drawerLayout){
//        if (drawerLayout.isDrawerOpen(GravityCompat.END)){
//            drawerLayout.closeDrawer(GravityCompat.END);
//        }
//    }
//    public static void redirectActivity(Activity activity, Class secondActivity){
//        Intent intent = new Intent (activity, secondActivity);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        activity.startActivity(intent);
//        activity.finish();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        closedDrawer(drawerLayout);
//    }
//}
package com.example.notesgalery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton fab;
    private RecyclerView recyclerView;
    private ArrayList<DataClass> dataList;
    private MyAdapter adapter;
    private DatabaseReference databaseReference;
    Button edit;

    //navbar + toolbar
    TextView currentFullName, currentEmail;
    DrawerLayout drawerLayout;
    ImageView menu, close;
    LinearLayout favorite, notes, npn, settings, logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = findViewById(R.id.fab);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        dataList = new ArrayList<>();
        adapter = new MyAdapter(this, dataList);
        recyclerView.setAdapter(adapter);

        //navbar + toolbar
        drawerLayout = findViewById(R.id.drawerLayout);
        menu = findViewById(R.id.btMenu);
        favorite = findViewById(R.id.favorite);
        notes = findViewById(R.id.notes);
        npn = findViewById(R.id.npn);
        settings = findViewById(R.id.settings);
        logout = findViewById(R.id.logout);
        close = findViewById(R.id.btCloseNavbar);
        currentFullName = findViewById(R.id.fullName);
        currentEmail = findViewById(R.id.nav_email);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closedDrawer(drawerLayout);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(drawerLayout);
            }
        });
        notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreate();
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectActivity(MainActivity.this, SettingsActivity.class);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                firebaseAuth.signOut();
                redirectActivity(MainActivity.this, RegisterLoginActivity.class);
            }
        });

        // Ambil UID pengguna saat ini
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String currentUserUID = currentUser.getUid();

            // Rujukan database ke direktori gambar pengguna saat ini
            DatabaseReference imagesRef = FirebaseDatabase.getInstance().getReference("Images").child(currentUserUID);

            // Mendengarkan perubahan di direktori Images dan mengisi dataList
            imagesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    dataList.clear();
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                        DataClass dataClass = dataSnapshot.getValue(DataClass.class);
                        dataClass.setId(dataSnapshot.getKey());
                        dataList.add(dataClass);
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    // Handle any errors if necessary
                }
            });

            // Rujukan database ke direktori data pengguna (Users)
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid());

            //fullname
            usersRef.child("fullName").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String fullName = dataSnapshot.getValue(String.class);
                    if (fullName != null) {
                        // Set teks fullName pada TextView atau komponen yang sesuai
                        currentFullName.setText(fullName);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Handle error jika terjadi kesalahan saat mengambil data dari Firebase
                }
            });

            //email
            usersRef.child("email").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String email = dataSnapshot.getValue(String.class);
                    if (email != null) {
                        // Set teks email pada TextView atau komponen yang sesuai
                        currentEmail.setText(email);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Handle error jika terjadi kesalahan saat mengambil data dari Firebase
                }
            });
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, UploadActivity.class);
                startActivity(intent);
            }
        });
    }

    //navbar + toolbar
    public static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.END);
    }

    public static void closedDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        }
    }

    public static void redirectActivity(Activity activity, Class secondActivity) {
        Intent intent = new Intent(activity, secondActivity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closedDrawer(drawerLayout);
    }
}
